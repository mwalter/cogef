====
Nemo
====

Information about `nemo <http://www.hpc.uni-freiburg.de/nemo>`__.

We assume that the installation will be located in ``$HOME/source``. If you
use GPAW, you can first follow the introduction on the GPAW_ website.

.. _GPAW: https://wiki.fysik.dtu.dk/gpaw/dev/platforms/Linux/nemo.html

Clone it and create a module
==================================

We create a place for cogef and get the trunk version::

 cd
 COGEF_SOURCE=$PWD/source/cogef
 mkdir -p $COGEF_SOURCE
 cd $COGEF_SOURCE
 git clone https://gitlab.com/cogef/cogef.git trunk

The current trunk version can then be updated by::

 cd $COGEF_SOURCE/trunk
 git pull

A specific tag can be loaded by::

 cd $COGEF_SOURCE/trunk
 # list tags
 git tag
 # load version 1.0.0
 git checkout v1.0.0

We create a module that does the necessary things::

  cd
  mkdir -p modulefiles/cogef
  cd modulefiles/cogef

We create the file :file:`trunk` that should read::

  #%Module1.0

  if {![is-loaded ase]} {module load ase}

  # change this to your path
  set cogefhome /home/fr/fr_fr/fr_ob1006/source/cogef/trunk
  prepend-path       PYTHONPATH    $cogefhome
  prepend-path       PATH          $cogefhome/bin

Load the module::

  module load cogef/trunk

And add the same line in the ``~/.bashrc`` behind other load commands

Testing
=======

Please run the tests::

  $ cogef test

When errors occur, check the requirements of COGEF (in particular the ASE
version).
