.. _tutorials:

=========
Tutorials
=========

We want to simulate external forces by the appication of 
geometry constraints. This "COnstraint GEometry to simulate Forces"
is known as the COGEF method [Beyer2000]_.

.. toctree::

    cogef1s/AuAg2/AuAg2
    cogef1s/AgAuAg/AgAuAg
    cogef3s/H10

References
==========

.. [Beyer2000] Beyer, M. K. J. Chem. Phys. **112** (2000) 7307
.. [Brügner2018] Brügner, O.; Walter, M. Phys. Rev. Materials **2** (2018) 113603

