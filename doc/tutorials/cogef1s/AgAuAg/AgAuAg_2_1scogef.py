# creates: 1scogef.png
from ase import io
from ase.optimize import FIRE
from ase.calculators.emt import EMT

from cogef import COGEF1D

image = io.read('AgAuAg.traj')


def initialize(atoms):
    atoms.calc = EMT()
    return atoms

fmax = 0.01
cogef = COGEF1D(0, 2, initialize=initialize, fmax=fmax)

if not len(cogef.images):  # calculation was done already
    cogef.images = [image]

    stepsize = 0.1
    steps = 50
    cogef.move(stepsize, steps)
