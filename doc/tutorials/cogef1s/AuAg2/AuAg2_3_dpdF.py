import pylab as plt
from cogef.generalized import COGEF1D
from cogef import Dissociation
from cogef.dissociation.barrier import ElectronicBarrier

from cogef.units import nN

cogef = COGEF1D(0, 2)

# The force is assumed to increase uniformly by this loading rate.
alpha_nNs = 10  # [nN/s]
alpha = alpha_nNs * nN

diss = Dissociation(ElectronicBarrier(cogef))

fstep = 0.001
for T in [5, 50, 100, 200, 300, 500]:  # Temperature [K]
    # Automatic limits
    fmin, fmax = diss.get_force_limits(T, alpha, force_step=fstep,
                                       method='electronic')
    dpdf, F = diss.probability_density(
        T, loading_rate=alpha, force_max=fmax, force_min=fmin,
        force_step=fstep)
    p = plt.plot(F / nN, dpdf * nN, 'o-',
                 label='T={0}K'.format(T))
    plt.axvline(x=cogef.get_maximum_force(method='use_energies') / nN,
                ls='--', color='k')

plt.legend(loc=2)
plt.ylim(0, 110)
plt.xlabel('F [nN]')
plt.ylabel('dp/dF [nN$^{-1}$]')
plt.plot()

plt.savefig('1scogef.png')
