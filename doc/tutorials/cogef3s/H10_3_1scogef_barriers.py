# creates: 1scogef_error.png
from ase.io.jsonio import write_json
from cogef import COGEF, Dissociation

cogef = COGEF(0, 8)

T = 300      # Temperature [K]
P = 101325.  # Pressure    [Pa]
force_min = 0.1
force_max = 4.
force_step = 0.1

diss = Dissociation(cogef, force_unit='nN')
rates, forces = diss.get_rate_constants(
    T, P, force_max, force_min, force_step, method='electronic')

barriers = []
for f_ext in forces:
    barriers.append(diss.electronic_energy_barrier(f_ext))

with open('1scogef_barriers.json', 'w') as f:
    write_json(f, {'forces': forces, 'barriers': barriers})
