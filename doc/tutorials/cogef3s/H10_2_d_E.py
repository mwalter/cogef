# creates: 1scogef_problem.png
import matplotlib.pyplot as plt
from cogef import COGEF1D

cogef = COGEF1D(0, 8)
energies = cogef.get_energies()
distances = cogef.get_distances()

plt.plot(distances, energies)
plt.xlabel(r'd [$\rm\AA$]')
plt.ylabel('U [eV]')
plt.savefig('1scogef_problem.png')
