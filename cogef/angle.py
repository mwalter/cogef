from typing import List
import numpy as np

from ase import Atoms
from ase.constraints import FixInternals

from cogef.generalized import COGEF1D
from cogef.rotate import rotate_around_axis, connected_to_1_if_0_removed


class AngleRestrictor(COGEF1D):
    def __init__(self, indices: List[int], **kwargs):
        super().__init__(0, 0, **kwargs)
        self.indices = np.array(indices, dtype=int)
        self.name = kwargs.get('name', None)  # XXX hack

    def _set_indices(self) -> None:
        if hasattr(self, 'connected_indices0'):
            return

        atoms = self.images[0]

        i0, i1, i2 = self.indices
        self.connected_indices0 = connected_to_1_if_0_removed(
            atoms, i1, i0)
        self.connected_indices2 = connected_to_1_if_0_removed(
            atoms, i1, i2)
        n_parts = (len(self.connected_indices0)
                   + len(self.connected_indices2))

        if n_parts >= len(atoms):
            # more atoms in parts
            # -> can not figure out the parts
            self.connected_indices0 = [i0]
            self.connected_indices2 = [i2]

    def get_constraint(self, atoms: Atoms):
        angle_deg = atoms.get_angle(*self.indices)
        return FixInternals(angles_deg=[[angle_deg, self.indices]])

    def shift_atoms(self, atoms: Atoms, dstep: float) -> None:
        pos_ac = atoms.get_positions()
        i0, i1, i2 = self.indices
        perp_c = np.cross(pos_ac[i0] - pos_ac[i1], pos_ac[i2] - pos_ac[i1])

        for ia in self.connected_indices0:
            pos_ac[ia] = pos_ac[i1] + rotate_around_axis(
                perp_c, pos_ac[ia] - pos_ac[i1], - dstep / 360 * np.pi)
        for ia in self.connected_indices2:
            pos_ac[ia] = pos_ac[i1] + rotate_around_axis(
                perp_c, pos_ac[ia] - pos_ac[i1], dstep / 360 * np.pi)

        atoms.set_positions(pos_ac, apply_constraint=False)

    def get_path(self):
        return np.array([img.get_angle(*self.indices)
                         for img in self.images])

    def move(self, dstep: float, steps: int):
        self._set_indices()
        super().move(dstep, steps)
