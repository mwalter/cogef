# Copyright (C) 2016-2019
# See accompanying license files for details.

"""Tests for cogef/cogef2d.py.
"""

from pathlib import Path
import pytest
import numpy as np

from ase import Atoms
from ase.optimize import FIRE
from ase.calculators.emt import EMT
from ase.calculators.morse import MorsePotential
from ase.io import Trajectory

from cogef.generalized import COGEF1D
from cogef.cogef2d import COGEF2D, FixedLength2D


def H3_EMT(fmax):
    """Create H3 optimized in EMT"""
    image = Atoms('H3', positions=[(0, 0, 0), (0.751, 0, 0), (0, 1., 0)])
    image.calc = EMT()
    FIRE(image, logfile=None).run(fmax=fmax)
    return image


def H4_linear_Morse_relaxed(fmax):
    """Create linear H4 optimzed with Morse"""
    image = Atoms('H4', positions=[(i, 0, 0) for i in range(4)])
    image.calc = MorsePotential()
    FIRE(image, logfile=None).run(fmax=fmax)
    return image


def energies(trajname):
    traj = Trajectory(trajname)
    return np.array([atoms.get_potential_energy() for atoms in traj])


def cogef_linear_H4(fix_force):
    fmax = 0.05
    images = [H4_linear_Morse_relaxed(fmax)]
    pull_atoms = [0, 3]
    break_atoms = [1, 2]

    name = 'H4_cogef2d'
    cogef2d = COGEF2D(pull_atoms, break_atoms,
                      name=name,
                      optimizer=FIRE, fmax=fmax, optimizer_logfile=None,
                      fix_force=fix_force,
                      max_image_number=40)
    cogef2d.images = images

    def initialize(image):
        image.calc = MorsePotential()
        return image

    # pull on outer atoms first

    steps = 20
    stepsize = 0.1
    cogef2d.pull(stepsize, steps, initialize)
    assert Path(name + '_{0}_{1}'.format(*pull_atoms)).is_dir()

    return cogef2d, initialize


def test_linear_fixed_force():
    cogef2d, initialize = cogef_linear_H4(fix_force=True)

    # find maximum for an intermediate configuration

    i = 10
    stepsize = 0.05

    # find maximum
    cogef2d.calc_maximum_curve([i], stepsize, initialize=initialize)
    # default name
    breaktrajectory = (Path(cogef2d.name) / 'pull_ff_{0}'.format(i)
                       / 'cogef_{0}_{1}'.format(1, 2)
                       / 'cogef.traj')
    enes = energies(breaktrajectory)
    assert len(enes) == 2
    assert enes.max() == pytest.approx(-3.523136325599446, 1e-4)

    # continue to find minimum
    cogef2d.calc_maximum_curve([i], stepsize,
                               initialize=initialize,
                               and_minimum_curve=True)
    enes = energies(breaktrajectory)
    assert len(enes) == 3
    assert enes.max() == pytest.approx(-3.523136325599446, 1e-4)
    assert enes.min() == pytest.approx(-4.702848925775359, 1e-4)

    emax, dmax = cogef2d.get_maximum_energy_curve()
    assert enes.max() == pytest.approx(
        emax[0] - cogef2d.f_ext * dmax[0], 1e-4)
    emin, dmin = cogef2d.get_minimum_energy_curve()
    print('emin, dmin=', emin, dmin, emin[0] - cogef2d.f_ext * dmin[0])
    # XXX why does this fail?
    # assert enes.min() == pytest.approx(
    # emin[0] - cogef2d.f_ext * dmin[0], 1e-4)

    cogef2d.calc_maximum_curve([8, 9], stepsize, initialize=initialize)
    assert (Path(cogef2d.name) / 'pull_ff_9').is_dir()


def test_linear_fixed_d():
    cogef2d, initialize = cogef_linear_H4(fix_force=False)

    # find maximum for an intermediate configuration

    i = 18
    stepsize = 0.05

    # find maximum
    cogef2d.calc_maximum_curve([i], stepsize,
                               initialize=initialize)
    breaktrajectory = (Path(cogef2d.name) / 'pull_fd_{0}'.format(i)
                       / 'cogef_{0}_{1}'.format(1, 2)
                       / 'cogef.traj')  # default
    enes = energies(breaktrajectory)
    assert len(enes) == 15
    # these are E - F * d values
    assert enes.max() == pytest.approx(-0.16171266334027795, 1e-4)

    # continue to find minimum
    cogef2d.calc_maximum_curve([i], stepsize,
                               initialize=initialize,
                               max_trajectory='pull_max.traj',
                               and_minimum_curve=True,
                               min_trajectory='pull_min.traj',)
    enes = energies(breaktrajectory)
    assert len(enes) == 38
    # these are E - F * d values
    assert enes.max() == pytest.approx(-0.16171266334027795, 1e-4)
    assert enes.min() == pytest.approx(-2.0000383106420445, 1e-4)


def test_fixed_d(H4_cogef1d):
    break_atoms = [1, 2]
    propagator = COGEF1D(*break_atoms)

    cogef2d = FixedLength2D(H4_cogef1d, propagator)

    # find maximum for an intermediate configuration

    i = 18
    stepsize = 0.05

    cogef2d.find_barrier(stepsize, i)

    breaktrajectory = (Path(cogef2d.cogef1d.name) / f'fd_{i}'
                       / 'cogef1d_{0}_{1}'.format(1, 2)
                       / 'cogef.traj')  # default
    enes = energies(breaktrajectory)
    assert len(enes) == 15
    # these are E - F * d values
    assert enes.max() == pytest.approx(-0.16171266334027795, 1e-4)

    cogef2d.find_barrier(stepsize, i)
    assert len(propagator.images) == len(enes)


def test_start_from_1S(H4_cogef1d):
    """Test to start 3S from a previous 1S calculation."""
    fmax = 0.05
    break_atoms = [1, 2]

    # pull 1S-COGEF first

    cogef1d = H4_cogef1d
    name = cogef1d.name.split('_')[0]
    pull_atoms = [cogef1d.atom1, cogef1d.atom2]

    # define 3S-COGEF

    cogef2d = COGEF2D(pull_atoms, break_atoms,
                      name=name,
                      optimizer=FIRE, fmax=fmax, optimizer_logfile=None,
                      fix_force=True,
                      max_image_number=40)

    def initialize(image):
        image.calc = MorsePotential()
        return image

    i = 10
    stepsize = 0.05

    # find maximum
    cogef2d.calc_maximum_curve([i], stepsize,
                               initialize=initialize)
    breaktrajectory = (Path(cogef2d.name) / 'pull_ff_{0}'.format(i)
                       / 'cogef_{0}_{1}'.format(*break_atoms)
                       / 'cogef.traj')  # default
    enes = energies(breaktrajectory)
    assert len(enes) == 2
    assert enes.max() == pytest.approx(-3.523136325599446, 1e-4)


def test_cogef2d():
    # Class COGEF2D
    fmax = 0.05

    pull_atoms = [0, 1]
    break_atoms = [0, 2]
    steps = 10
    stepsize = 0.25

    images = [H3_EMT(fmax)]

    def initialize(image):
        image.calc = EMT()
        return image

    cogef2d = COGEF2D(pull_atoms, break_atoms,
                      name='cogef2d',
                      optimizer=FIRE, fmax=fmax)
    cogef2d.images = images
    # obtain the 1d result = "reactant curve"
    cogef2d.pull(stepsize, steps, initialize)
    # try to find the maximum
    cogef2d.calc_maximum_curve([9], 0.1, initialize=initialize,
                               max_trajectory='pull_max.traj',
                               breakdirectory='pull',
                               min_trajectory='pull_min.traj',)
