from pathlib import Path
import numpy as np

from ase import Atoms
from ase.optimize import FIRE
from ase.calculators.emt import EMT
from ase.calculators.morse import MorsePotential
from ase.io import Trajectory

from cogef.cogef2d import COGEF2D
from cogef.cogef2d import Dissociation2d


def H3_EMT(fmax):
    """Create H3 optimized in EMT"""
    image = Atoms('H3', positions=[(0, 0, 0), (0.751, 0, 0), (0, 1., 0)])
    image.calc = EMT()
    FIRE(image, logfile=None).run(fmax=fmax)
    return image


def H4_linear_Morse(fmax):
    """Create linear H4 optimzed with Morse"""
    image = Atoms('H4', positions=[(i, 0, 0) for i in range(4)])
    image.calc = MorsePotential()
    FIRE(image, logfile=None).run(fmax=fmax)
    return image


def energies(trajname):
    traj = Trajectory(trajname)
    return np.array([atoms.get_potential_energy() for atoms in traj])


def cogef_linear_H4(tmp_path, fmax, fix_force):
    images = [H4_linear_Morse(fmax)]
    pull_atoms = [0, 3]
    break_atoms = [1, 2]

    name = str(tmp_path / 'cogef2d')
    cogef = COGEF2D(pull_atoms, break_atoms,
                    name=name,
                    optimizer=FIRE, fmax=fmax, optimizer_logfile=None,
                    fix_force=fix_force,
                    max_image_number=40)
    cogef.images = images

    def initialize(image):
        """Initialize the image and return the trajectory name."""
        image.calc = MorsePotential()
        return image

    # pull on outer atoms first

    steps = 40
    stepsize = 0.1
    cogef.pull(stepsize, steps, initialize)
    assert Path(name + '_{0}_{1}'.format(*pull_atoms)).is_dir()

    return cogef, initialize


def test_call(tmp_path):
    cogef, initialize = cogef_linear_H4(tmp_path, fmax=0.05, fix_force=True)

    diss = Dissociation2d(cogef,
                          transition_into_two_fragments=True)

    try:
        force_ext = 3.0
        pmax, pmin = diss.electronic_extreme_values(force_ext)
    except ValueError:
        # XXXX this fails, as some parameters are not matching
        pass
