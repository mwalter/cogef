from pathlib import Path
import pytest
import numpy as np

from ase import Atoms
from ase.optimize import FIRE
from ase.calculators.morse import MorsePotential
from ase.io import Trajectory

from cogef.generalized import COGEF1D
from cogef.cogef2d import FixedLength2D


def H4_linear_Morse_relaxed(fmax):
    """Create linear H4 optimzed with Morse"""
    image = Atoms('H4', positions=[(i, 0, 0) for i in range(4)])
    image.calc = MorsePotential()
    FIRE(image, logfile=None).run(fmax=fmax)
    return image


def energies(trajname):
    traj = Trajectory(trajname)
    return np.array([atoms.get_potential_energy() for atoms in traj])


@pytest.fixture
def cogef_linear_H4():
    pull_atoms = [0, 3]
    fmax = 0.05
    cogef = COGEF1D(*pull_atoms, fmax=fmax, optimizer_logfile=None)
    cogef.images = [H4_linear_Morse_relaxed(fmax)]
    steps = 20
    stepsize = 0.1
    cogef.move(stepsize, steps)

    def initialize(image):
        image.calc = MorsePotential()
        return image

    break_atoms = [1, 2]
    cogef2 = COGEF1D(*break_atoms, fmax=fmax, initialize=initialize,
                     optimizer_logfile=None)

    cogef3s = FixedLength2D(cogef, cogef2)

    return cogef3s, initialize


def test_linear_fixed_d(cogef_linear_H4):
    cogef2d, initialize = cogef_linear_H4

    # find maximum for an intermediate configuration

    i = 18
    stepsize = 0.05

    # find maximum
    cogef2d.find_barrier(stepsize, i)
    # default name
    breaktrajectory = (Path(cogef2d.cogef1d.name) / 'fd_{0}'.format(i)
                       / 'cogef1d_{0}_{1}'.format(1, 2)
                       / 'cogef.traj')
    enes = energies(breaktrajectory)
    assert len(enes) == 15
    # these are E - F * d values
    assert enes.max() == pytest.approx(-0.16171266334027795, 1e-4)

    # continue to find minimum
    cogef2d.find_minimum(stepsize, [i])
    enes = energies(breaktrajectory)
    assert len(enes) == 38
    # these are E - F * d values
    assert enes.max() == pytest.approx(-0.16171266334027795, 1e-4)
    assert enes.min() == pytest.approx(-2.0000383106420445, 1e-4)


def test_fixed_d(H4_cogef1d):
    break_atoms = [1, 2]
    propagator = COGEF1D(*break_atoms)

    cogef2d = FixedLength2D(H4_cogef1d, propagator)

    # find maximum for an intermediate configuration

    i = 18
    stepsize = 0.05

    cogef2d.find_barrier(stepsize, i)

    breaktrajectory = (Path(cogef2d.cogef1d.name) / f'fd_{i}'
                       / 'cogef1d_{0}_{1}'.format(1, 2)
                       / 'cogef.traj')  # default
    enes = energies(breaktrajectory)
    assert len(enes) == 15
    # these are E - F * d values
    assert enes.max() == pytest.approx(-0.16171266334027795, 1e-4)

    cogef2d.find_barrier(stepsize, i)
    assert len(propagator.images) == len(enes)
