import ase.units as aseu
from cogef.units import nN


def test_units():
    assert nN == 1e-9 * aseu.J / aseu.m
