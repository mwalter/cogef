import pytest
from ase.build import molecule
from ase.calculators.emt import EMT

from cogef.angle import AngleRestrictor


def test_rotate():
    atoms = molecule('isobutane')
    atoms.calc = EMT()

    dphi_deg = 10
    indices = [6, 0, 2]
    restrictor = AngleRestrictor(indices)
    restrictor.images = [atoms]
    restrictor.move(dphi_deg, 2)

    path_i = restrictor.get_path()
    for i in range(1, len(path_i)):
        assert path_i[i - 1] + dphi_deg == pytest.approx(path_i[i])
