import pytest
import numpy as np

from ase.build import molecule

from cogef.rotate import (
    get_angles, rotate_around_axis, splitted_indices,
    connected_to_1_if_0_removed)


def test_angles():
    r = 4
    theta = np.pi / 3

    for phi in [np.pi / 2, - np.pi / 2]:
        ph, th = get_angles(r * np.array([np.cos(phi) * np.sin(theta),
                                          np.sin(phi) * np.sin(theta),
                                          np.cos(theta)]))
        assert th == pytest.approx(theta)
        assert ph == pytest.approx(phi)

    # undefined angles
    ph, th = get_angles(np.zeros(3))
    assert ph == 0
    assert th == 0


def test_rotate():
    axis = np.ones(3)

    r = 3
    vector = r * np.array([1, 0, 0])
    assert np.allclose(rotate_around_axis(axis, vector, np.pi),
                       np.array([-1, 2, 2]))


def test_split_biphenyl():
    biphenyl = molecule('biphenyl')
    dihedral = [1, 0, 14, 15]

    indices1, indices2 = splitted_indices(biphenyl, *dihedral[1:3])
    # all indices
    assert len(indices1) + len(indices2) == len(biphenyl)
    # no duplicates
    assert len(set(indices1) & set(indices2)) == 0


def test_isobutane_arms():
    """Check that the arm lengths are correctly reproduced"""
    atoms = molecule('isobutane')

    for i1 in [2, 6, 10]:
        assert len(connected_to_1_if_0_removed(atoms, 0, i1)) == 4
